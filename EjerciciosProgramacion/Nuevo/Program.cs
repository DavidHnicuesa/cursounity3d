﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuevo
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1, producto;
            string linea;
            Console.Write("Ingrese tamaño del lado del cuadrado:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            producto = num1 * 4;
            Console.Write("El perímetro del cuadrado es:");
            Console.WriteLine(producto);
            Console.ReadKey();
        }
    }
}
