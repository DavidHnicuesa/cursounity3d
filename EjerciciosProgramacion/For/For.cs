﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace For
{
    //Realizar un programa que imprima en pantalla los números del 1 al 100.
    class Program
    {
        static void Main(string[] args)
        {
            //Definimos la variable f
            int f;
            /*Iniciamos el bucle for, ponemos la condición entre parentesis, en tres partes separadas por punto y coma.
             * En la primera parte le asignamos el valor a f que en este caso es uno, que es el número por el que vamos a empezar.
             * En la segundo parte le asignamos el valor máximo que puede alcanzar f en este caso es igual a 100, lo repetirá el bucle hasta que el valor de f sea mayor de 100, en ese momento dejará de entrar y finalizará el programa.
             * En la tercera parte, se le indica la operación que debe realizar, en este caso, sumará uno al número anterior que tiene asignado f.
             
             */
            for (f = 1; f <= 100; f++)
            {
                //Escribe el valor de f.
                Console.Write(f);
                //Escribe un espacio, un guión y un espacio para separar los números que escribe.
                Console.Write("-");
            }
            Console.ReadKey();
        }
    }
}

/*Ejemplos:
 * 1.- Desarrollar un programa que permita la carga de 10 valores por teclado y nos muestre posteriormente la suma de los valores ingresados y su promedio. Este problema ya lo desarrollamos, lo resolveremos empleando la estructura for.
 * 
 * using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor2
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma,f,valor,promedio;
            string linea;
            suma=0;
            for(f=1;f<=10;f++) 
            {
                Console.Write("Ingrese valor:");
                linea=Console.ReadLine();
                valor=int.Parse(linea);
                suma=suma+valor;
            }
            Console.Write("La suma es:");
            Console.WriteLine(suma);
            promedio=suma/10;
            Console.Write("El promedio es:");
            Console.Write(promedio);
            Console.ReadKey();
        }
    }
}
 
 
2.- Escribir un programa que lea 10 notas de alumnos y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor3
{
    class Program
    {
        static void Main(string[] args)
        {
            int aprobados,reprobados,f,nota;
            string linea;
            aprobados=0;
            reprobados=0;
            for(f=1;f<=10;f++) 
            {
                Console.Write("Ingrese la nota:");
                linea = Console.ReadLine();
                nota=int.Parse(linea);
                if (nota>=7) 
                {
                    aprobados=aprobados+1;
                }
                else
                {
            	    reprobados=reprobados+1;
                }
            }
            Console.Write("Cantidad de aprobados:");
            Console.WriteLine(aprobados);
            Console.Write("Cantidad de reprobados:");
            Console.Write(reprobados);
            Console.ReadKey();
        }
    }
}

3.- Escribir un programa que lea 10 números enteros y luego muestre cuántos valores ingresados fueron múltiplos de 3 y cuántos de 5. Debemos tener en cuenta que hay números que son múltiplos de 3 y de 5 a la vez.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor4
{
    class Program
    {
        static void Main(string[] args)
        {
            int mul3,mul5,valor,f;
            string linea;
            mul3=0;
            mul5=0;
            for(f=1;f<=10;f++) 
            {
                Console.Write("Ingrese un valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                if (valor%3==0) 
                {
                    mul3=mul3+1;
                } 
                if (valor%5==0) 
                {
                    mul5=mul5+1;
                }
            }
            Console.Write("Cantidad de valores ingresados múltiplos de 3:");
            Console.WriteLine(mul3);
            Console.Write("Cantidad de valores ingresados múltiplos de 5:");
            Console.Write(mul5);
            Console.ReadKey();
        }
    }
}

4.- Escribir un programa que lea n números enteros y calcule la cantidad de valores mayores o iguales a 1000.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor5
{
    class Program
    {
        static void Main(string[] args)
        {
            int cantidad,n,f,valor;
            string linea;
            cantidad=0;
            Console.Write("Cuantos valores ingresará:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            for(f=1;f<=n;f++) 
            {
                Console.Write("Ingrese el valor:");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                if (valor>=1000) 
                {
                    cantidad=cantidad+1;
                }
            }
            Console.Write("La cantidad de valores ingresados mayores o iguales a 1000 son:");
            Console.Write(cantidad);
            Console.ReadKey();
        }
    }
}


5.- Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la medida de la base y la altura de un triángulo. El programa deberá informar:
a) De cada triángulo la medida de su base, su altura y su superficie.
b) La cantidad de triángulos cuya superficie es mayor a 12. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor6
{
    class Program
    {
        static void Main(string[] args)
        {
            int basetri,altura,superficie,cantidad,f,n;
            string linea;
            cantidad=0;
            Console.Write("Cuantos triángulos procesará:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            for(f=1;f<=n;f++) 
            {
                Console.Write("Ingrese el valor de la base:");
                linea = Console.ReadLine();

                basetri=int.Parse(linea);
                Console.Write("Ingrese el valor de la altura:");
                linea = Console.ReadLine();
                altura=int.Parse(linea);
                superficie=basetri*altura/2;
                Console.Write("La superficie es:");
                Console.WriteLine(superficie);
                if (superficie>12) 
                {
                    cantidad=cantidad+1;
                }
            }
            Console.Write("La cantidad de triángulos con superficie superior a 12 son:");
            Console.Write(cantidad);
            Console.ReadKey();
        }
    }
}

6.- Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos 5 valores ingresados.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor7
{
    class Program
    {
        static void Main(string[] args)
        {
            int f,valor,suma;
            string linea;
            suma=0;
            for(f=1;f<=10;f++) 
            {
                Console.Write("Ingrese un valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                if (f>5) 
                {
                    suma=suma+valor;
                }
            }
            Console.Write("La suma de los últimos 5 valores es:");
            Console.Write(suma);
            Console.ReadKey();
        }
    }
}

7.- Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor8
{
    class Program
    {
        static void Main(string[] args)
        {
            int f;
            for(f=5;f<=50;f=f+5) 
            {
	            Console.Write(f);
	            Console.Write("-");
            }
            Console.ReadKey();
        }
    }
}

8.- Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 13 términos)
Ejemplo: Si ingreso 3 deberá aparecer en pantalla los valores 3, 6, 9, hasta el 39.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor9
{
    class Program
    {
        static void Main(string[] args)
        {
            int f,valor;
            string linea;
            Console.Write("Ingrese un valor entre 1 y 10:");
            linea = Console.ReadLine();
            valor=int.Parse(linea);
            for(f=valor;f<=valor*12;f=f+valor) 
            {
                Console.Write(f);
                Console.Write("-");
            }
            Console.ReadKey();
        }
    }
}

9.- Realizar un programa que lea los lados de n triángulos, e informar:
a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales), o escaleno (ningún lado igual)
b) Cantidad de triángulos de cada tipo.
c) Tipo de triángulo que posee menor cantidad.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor10
{
    class Program
    {
        static void Main(string[] args)
        {
            int f,lado1,lado2,lado3,cant1,cant2,cant3,n;
            string linea;
            cant1=0;
            cant2=0;
            cant3=0;
            Console.Write("Ingrese la cantidad de triángulos:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            for(f=1;f<=n;f++) 
            {
               Console.Write("Ingrese lado 1:");
               linea = Console.ReadLine();
               lado1=int.Parse(linea);
               Console.Write("Ingrese lado 2:");
               linea = Console.ReadLine();
               lado2 = int.Parse(linea);
               Console.Write("Ingrese lado 3:");
               linea = Console.ReadLine();
               lado3 = int.Parse(linea);
               if (lado1==lado2 && lado1==lado3) 
               {
                   Console.WriteLine("Es un triángulo equilatero.");
                   cant1++;
               }
               else 
               {
                   if (lado1==lado2 || lado1==lado3 || lado2==lado3) 
                   {
                       Console.WriteLine("Es un triángulo isósceles.");
                       cant2++;
                   }
                   else
                   {
            	       cant3++;
                       Console.WriteLine("Es un triángulo escaleno.");
                   }
               }
            }
            Console.Write("Cantidad de triángulos equilateros:");
            Console.WriteLine(cant1);
            Console.Write("Cantidad de triángulos isósceles:");
            Console.WriteLine(cant2);
            Console.Write("Cantidad de triángulos escalenos:");
            Console.WriteLine(cant3);
            if (cant1<cant2 && cant1<cant3) 
            {
                Console.Write("Hay menor cantidad de triángulos equilateros.");
            }
            else
            {
                if (cant2<cant3) 
                {
                    Console.Write("Han menor cantidad de triángulos isósceles");	
                }
                else
                {
                    Console.Write("Han menor cantidad de triángulos escalenos");	                
                }
            }
            Console.ReadKey();
        }
    }
}

10.- Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor11
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,f,x,y,cant1,cant2,cant3,cant4;
            string linea;
            cant1=0;
            cant2=0;
            cant3=0;
            cant4=0;
            Console.Write("Cantidad de puntos:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            for(f=1;f<=n;f++) 
            {
                Console.Write("Ingrese coordenada x:");
                linea = Console.ReadLine();
                x=int.Parse(linea);
                Console.Write("Ingrese coordenada y:");
                linea = Console.ReadLine();
                y=int.Parse(linea);
                if (x>0 && y>0) 
                {
                    cant1++;
                }
                else
                {
                    if (x<0 && y>0) 
                    {
                        cant2++;
                    }
                    else
                    {
                        if (x<0 && y<0) 
                        {
                            cant3++;
                        }
                        else
                        {
                            if (x>0 && y<0) 
                            {
                        	cant4++;
                            }
                        }
                    }
                }
            }
            Console.Write("Cantidad de puntos en el primer cuadrante:");
            Console.WriteLine(cant1);
            Console.Write("Cantidad de puntos en el segundo cuadrante:");
            Console.WriteLine(cant2);
            Console.Write("Cantidad de puntos en el tercer cuadrante:");
            Console.WriteLine(cant3);
            Console.Write("Cantidad de puntos en el cuarto cuadrante:");
            Console.WriteLine(cant4);
            Console.ReadKey();
        }
    }
}

11.- Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
a) La cantidad de valores ingresados negativos.
b) La cantidad de valores ingresados positivos.
c) La cantidad de múltiplos de 15.
d) El valor acumulado de los números ingresados que son pares.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor12
{
    class Program
    {
        static void Main(string[] args)
        {
            int f,valor,negativos,positivos,mult15,sumapares;
            string linea;
            negativos=0;
            positivos=0;
            mult15=0;
            sumapares=0;
            for(f=1;f<=10;f++) 
            {
                Console.Write("Ingrese valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                if (valor<0) 
                {
                    negativos++;
                }
                else
                {
            	    if (valor>0) 
                    {
            		    positivos++;
            	    }
                }
                if (valor%15==0) 
                {
            	    mult15++;
                }
                if (valor%2==0) 
                {
            	    sumapares=sumapares+valor;
                }
            }
            Console.Write("Cantidad de valores negativos:");
            Console.WriteLine(negativos);
            Console.Write("Cantidad de valores positivos:");
            Console.WriteLine(positivos);
            Console.Write("Cantidad de valores múltiplos de 15:");
            Console.WriteLine(mult15);
            Console.Write("Suma de los valores pares:");
            Console.WriteLine(sumapares);
            Console.ReadKey();
        }
    }
}

12.- Se cuenta con la siguiente información:
Las edades de 50 estudiantes del turno mañana.
Las edades de 60 estudiantes del turno tarde.
Las edades de 110 estudiantes del turno noche.
Las edades de cada estudiante deben ingresarse por teclado.
a) Obtener el promedio de las edades de cada turno (tres promedios)
b) Imprimir dichos promedios (promedio de cada turno)
c) Mostrar por pantalla un mensaje que indique cual de los tres turnos tiene un promedio de edades menor. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor13
{
    class Program
    {
        static void Main(string[] args)
        {
            int f,edad,suma1,suma2,suma3,pro1,pro2,pro3;
            string linea;
            suma1=0;
            suma2=0;
            suma3=0;
            for(f=1;f<=50;f++) 
            {
                Console.Write("Ingrese edad:");
                linea = Console.ReadLine();
                edad=int.Parse(linea);
                suma1=suma1+edad;
            }
            pro1=suma1/50;
            Console.Write("Promedio de edades del turno mañana:");
            Console.WriteLine(pro1);
            for(f=1;f<=60;f++)
            {
                Console.Write("Ingrese edad:");
                linea = Console.ReadLine();
                edad = int.Parse(linea) ;
                suma2=suma2+edad;
            }
            pro2=suma2/60;
            Console.Write("Promedio de edades del turno tarde:");
            Console.WriteLine(pro2);
            for(f=1;f<=110;f++) 
            {
                Console.Write("Ingrese edad:");
                linea = Console.ReadLine();
                edad=int.Parse(linea);
                suma3=suma3+edad;
            }
            pro3=suma3/110;
            Console.Write("Promedio de edades del turno noche:");
            Console.WriteLine(pro3);
            if (pro1<pro2 && pro1<pro3) 
            {
                Console.Write("El turno mañana tiene un promedio menor de edades.");
            }
            else
            {
                if (pro2<pro3) 
                {
                    Console.Write("El turno tarde tiene un promedio menor de edades.");                
                }
                else
                {
                    Console.Write("El turno noche tiene un promedio menor de edades.");
                }
            }
            Console.ReadKey();
        }
    }
}

 */
