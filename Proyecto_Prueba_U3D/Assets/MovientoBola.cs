﻿using UnityEngine;

public class MovientoBola : MonoBehaviour
{
    int chocquesPuerta;

    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log(transform.gameObject.name);
        // transform.position.x = 10;
        transform.position = new Vector3(0, 5, 0);
        chocquesPuerta = 3;
    }

    // Update is called once per frame
    void Update() {
       /* Provoca una excepción:     GameObject vacio = null;
        vacio.transform.position = Vector3.zero;
       */
        bool siFlechaIzquierdaPulsada = Input.GetKey(KeyCode.LeftArrow);

        if (siFlechaIzquierdaPulsada) {

            transform.position = new Vector3(transform.position.x - 0.07f,
                transform.position.y,
                0f);
        }
        bool siFlechaDerechaPulsada = Input.GetKey(KeyCode.RightArrow);


        if (siFlechaDerechaPulsada) {

            transform.position = new Vector3(transform.position.x + 0.07f,
                transform.position.y,
                0f);
        }
    }

    /*
    void OnTriggerEnter(Collider collider) {
        Debug.Log("OnTriggerEnter");
    }*/
    void OnCollisionEnter(Collision collision) {

        Debug.Log("OnCollisionEnter: " + collision.contacts[0].point.ToString()
            + " Nombre: " +collision.gameObject.name);

        if (collision.gameObject.name == "Objeto_Cubo") {
            chocquesPuerta--;
            if (chocquesPuerta == 0) {

                collision.gameObject.SetActive(false);
            }
        }
    }
}
